# nks

## service 
- `docker-compose.yml`
- jdbc + servlet and a fresh coat of kotlin paint
- dsls 
- functional http router dsl
- reactive + netty 
- flows
- coroutine + netty
- aot: go native kotlin, not kotlin native (but that's cool too)
- testing
  - testcontainers 
- change dev url to testcontainers, too


## client 
- kotlin.js 
